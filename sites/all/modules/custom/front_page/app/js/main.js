(function ($) {
  // Drupal.behaviors.kb_inf_report = {
  //   attach: function (context, settings) {
      $(document).ready(function(){

        var app = {

          data:null,

          curPos:[55.834,37.632],

          zoom: 13,

          start: function(data) {
            // this.getCurPosition();
            this.data = data;
            this.initMap();
          },

          initMap:function() {

            var map = L.map('map');
            map.setView(this.curPos, this.zoom);
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(map);
            var mar = L.marker(this.curPos).addTo(map);

            var data = this.data;

            $.each(data , function(){
              var myIcon = L.icon({
                iconUrl: this.icon,
                iconRetinaUrl: this.icon,
                shadowUrl: 'sites/all/modules/custom/front_page/app/img/marker-shadow.png',
                shadowRetinaUrl: 'sites/all/modules/custom/front_page/app/img/marker-shadow.png',
                iconSize: [16, 16],
                shadowSize: [38, 35],
                shadowAnchor: [12, 34]
              });

              var m = L.marker([this.lat, this.lon] , {icon:myIcon , clickable:true})
                  .on('click' , onClick)
                  .addTo(map);

              m.myOptions = this;

              function onClick () {
                window.location.href = window.location.href + '/' + this.myOptions.id;
              }

            });
          },

          renderCompanies: function () {

          },

          getCurPosition: function() {

            var self = this;

            var options = {
              enableHighAccuracy: true,
              timeout: 5000,
              maximumAge: 0
            };

            function success(pos) {
              var crd = pos.coords;
              self.curPos.lat =  crd.latitude;
              self.curPos.lat =  crd.longitude;
            };

            function error(err) {
              console.warn('ERROR(' + err.code + '): ' + err.message);
            };

            navigator.geolocation.getCurrentPosition(success, error, options);
          }
        };

        //download data
        $.ajax({
          url: "sites/all/modules/custom/front_page/app/d.json",
          success: function(data){
            app.start(data);
          }
        });

      });
  //   }
  // };
}(jQuery));