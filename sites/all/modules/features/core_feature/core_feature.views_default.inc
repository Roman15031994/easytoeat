<?php
/**
 * @file
 * core_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function core_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'menu';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Меню';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Меню';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Content: Картинка */
  $handler->display->display_options['fields']['field_menuelement_img']['id'] = 'field_menuelement_img';
  $handler->display->display_options['fields']['field_menuelement_img']['table'] = 'field_data_field_menuelement_img';
  $handler->display->display_options['fields']['field_menuelement_img']['field'] = 'field_menuelement_img';
  $handler->display->display_options['fields']['field_menuelement_img']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_menuelement_img']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Цена */
  $handler->display->display_options['fields']['field_menuelement_price']['id'] = 'field_menuelement_price';
  $handler->display->display_options['fields']['field_menuelement_price']['table'] = 'field_data_field_menuelement_price';
  $handler->display->display_options['fields']['field_menuelement_price']['field'] = 'field_menuelement_price';
  $handler->display->display_options['fields']['field_menuelement_price']['settings'] = array(
    'thousand_separator' => '',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Компания (field_menuelement_company) */
  $handler->display->display_options['arguments']['field_menuelement_company_target_id']['id'] = 'field_menuelement_company_target_id';
  $handler->display->display_options['arguments']['field_menuelement_company_target_id']['table'] = 'field_data_field_menuelement_company';
  $handler->display->display_options['arguments']['field_menuelement_company_target_id']['field'] = 'field_menuelement_company_target_id';
  $handler->display->display_options['arguments']['field_menuelement_company_target_id']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['field_menuelement_company_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_menuelement_company_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_menuelement_company_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_menuelement_company_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'menu_element' => 'menu_element',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'menu';
  $export['menu'] = $view;

  $view = new view();
  $view->name = 'profiles';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'profile';
  $view->human_name = 'Профили';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Профили';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Profile: Profile ID */
  $handler->display->display_options['fields']['pid']['id'] = 'pid';
  $handler->display->display_options['fields']['pid']['table'] = 'profile';
  $handler->display->display_options['fields']['pid']['field'] = 'pid';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'profiles';
  $export['profiles'] = $view;

  $view = new view();
  $view->name = 'profiles_view';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'profile';
  $view->human_name = 'Профили';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Profile: Profile ID */
  $handler->display->display_options['fields']['pid']['id'] = 'pid';
  $handler->display->display_options['fields']['pid']['table'] = 'profile';
  $handler->display->display_options['fields']['pid']['field'] = 'pid';
  /* Field: Profile: Название */
  $handler->display->display_options['fields']['field_company_name']['id'] = 'field_company_name';
  $handler->display->display_options['fields']['field_company_name']['table'] = 'field_data_field_company_name';
  $handler->display->display_options['fields']['field_company_name']['field'] = 'field_company_name';
  /* Field: Profile: ФИО */
  $handler->display->display_options['fields']['field_person_fullname']['id'] = 'field_person_fullname';
  $handler->display->display_options['fields']['field_person_fullname']['table'] = 'field_data_field_person_fullname';
  $handler->display->display_options['fields']['field_person_fullname']['field'] = 'field_person_fullname';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'profiles';
  $export['profiles_view'] = $view;

  $view = new view();
  $view->name = 'restaraunts';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'restaraunts';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Рестораны';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_restr_location_geoobject' => 'field_restr_location_geoobject',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_restr_location_geoobject' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Footer: Global: View area */
  $handler->display->display_options['footer']['view']['id'] = 'view';
  $handler->display->display_options['footer']['view']['table'] = 'views';
  $handler->display->display_options['footer']['view']['field'] = 'view';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_restar_company_target_id']['id'] = 'field_restar_company_target_id';
  $handler->display->display_options['relationships']['field_restar_company_target_id']['table'] = 'field_data_field_restar_company';
  $handler->display->display_options['relationships']['field_restar_company_target_id']['field'] = 'field_restar_company_target_id';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_suggestion_company_target_id']['id'] = 'field_suggestion_company_target_id';
  $handler->display->display_options['relationships']['field_suggestion_company_target_id']['table'] = 'field_data_field_suggestion_company';
  $handler->display->display_options['relationships']['field_suggestion_company_target_id']['field'] = 'field_suggestion_company_target_id';
  $handler->display->display_options['relationships']['field_suggestion_company_target_id']['label'] = '2 User entity referenced from field_suggestion_company';
  /* Relationship: User: Profile */
  $handler->display->display_options['relationships']['profile']['id'] = 'profile';
  $handler->display->display_options['relationships']['profile']['table'] = 'users';
  $handler->display->display_options['relationships']['profile']['field'] = 'profile';
  $handler->display->display_options['relationships']['profile']['relationship'] = 'field_restar_company_target_id';
  $handler->display->display_options['relationships']['profile']['bundle_types'] = array(
    'company' => 'company',
    'person' => 'person',
  );
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_field_suggestion_company_node']['id'] = 'reverse_field_suggestion_company_node';
  $handler->display->display_options['relationships']['reverse_field_suggestion_company_node']['table'] = 'users';
  $handler->display->display_options['relationships']['reverse_field_suggestion_company_node']['field'] = 'reverse_field_suggestion_company_node';
  $handler->display->display_options['relationships']['reverse_field_suggestion_company_node']['relationship'] = 'field_restar_company_target_id';
  /* Field: Field: Логотип */
  $handler->display->display_options['fields']['field_company_logo']['id'] = 'field_company_logo';
  $handler->display->display_options['fields']['field_company_logo']['table'] = 'field_data_field_company_logo';
  $handler->display->display_options['fields']['field_company_logo']['field'] = 'field_company_logo';
  $handler->display->display_options['fields']['field_company_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_company_logo']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Компания */
  $handler->display->display_options['fields']['field_restar_company']['id'] = 'field_restar_company';
  $handler->display->display_options['fields']['field_restar_company']['table'] = 'field_data_field_restar_company';
  $handler->display->display_options['fields']['field_restar_company']['field'] = 'field_restar_company';
  $handler->display->display_options['fields']['field_restar_company']['settings'] = array(
    'link' => 1,
  );
  /* Field: Content: Локация (GeoObject) */
  $handler->display->display_options['fields']['field_restr_location_geoobject']['id'] = 'field_restr_location_geoobject';
  $handler->display->display_options['fields']['field_restr_location_geoobject']['table'] = 'field_data_field_restr_location';
  $handler->display->display_options['fields']['field_restr_location_geoobject']['field'] = 'field_restr_location_geoobject';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'node';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'article' => 'article',
    'restaraunt' => 'restaraunt',
  );

  /* Display: Geo */
  $handler = $view->new_display('page', 'Geo', 'page');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'geofield_ymap';
  $handler->display->display_options['style_options']['map_center'] = '55.834105,37.632078';
  $handler->display->display_options['style_options']['map_zoom'] = '12';
  $handler->display->display_options['style_options']['map_auto_centering'] = TRUE;
  $handler->display->display_options['style_options']['map_auto_zooming'] = TRUE;
  $handler->display->display_options['style_options']['map_clusterize'] = TRUE;
  $handler->display->display_options['style_options']['hint_content_field'] = 'title';
  $handler->display->display_options['style_options']['additional_settings'] = array(
    'object_options' => '{
  "iconLayout": "default#image",
  "iconImageHref": "http://viberstickers.ru/data/emoticons/(burger).png",
  "iconImageSize": [24, 24],
  "iconImageOffset": [-3, -42]
}',
    'object_options_use_tokens' => 1,
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['footer'] = FALSE;
  /* Footer: Global: View area */
  $handler->display->display_options['footer']['view']['id'] = 'view';
  $handler->display->display_options['footer']['view']['table'] = 'views';
  $handler->display->display_options['footer']['view']['field'] = 'view';
  $handler->display->display_options['footer']['view']['label'] = 'table';
  $handler->display->display_options['footer']['view']['view_to_insert'] = 'restaraunts:restaur_table';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Field: Логотип */
  $handler->display->display_options['fields']['field_company_logo']['id'] = 'field_company_logo';
  $handler->display->display_options['fields']['field_company_logo']['table'] = 'field_data_field_company_logo';
  $handler->display->display_options['fields']['field_company_logo']['field'] = 'field_company_logo';
  $handler->display->display_options['fields']['field_company_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_company_logo']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Компания */
  $handler->display->display_options['fields']['field_restar_company']['id'] = 'field_restar_company';
  $handler->display->display_options['fields']['field_restar_company']['table'] = 'field_data_field_restar_company';
  $handler->display->display_options['fields']['field_restar_company']['field'] = 'field_restar_company';
  $handler->display->display_options['fields']['field_restar_company']['settings'] = array(
    'link' => 1,
  );
  /* Field: Content: Локация (GeoObject) */
  $handler->display->display_options['fields']['field_restr_location_geoobject']['id'] = 'field_restr_location_geoobject';
  $handler->display->display_options['fields']['field_restr_location_geoobject']['table'] = 'field_data_field_restr_location';
  $handler->display->display_options['fields']['field_restr_location_geoobject']['field'] = 'field_restr_location_geoobject';
  $handler->display->display_options['path'] = 'restaurants';

  /* Display: Table */
  $handler = $view->new_display('page', 'Table', 'restaur_table');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Field: Логотип */
  $handler->display->display_options['fields']['field_company_logo']['id'] = 'field_company_logo';
  $handler->display->display_options['fields']['field_company_logo']['table'] = 'field_data_field_company_logo';
  $handler->display->display_options['fields']['field_company_logo']['field'] = 'field_company_logo';
  $handler->display->display_options['fields']['field_company_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_company_logo']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'node';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['relationship'] = 'reverse_field_suggestion_company_node';
  $handler->display->display_options['fields']['title_1']['label'] = 'Предложения';
  /* Field: Content: Компания */
  $handler->display->display_options['fields']['field_restar_company']['id'] = 'field_restar_company';
  $handler->display->display_options['fields']['field_restar_company']['table'] = 'field_data_field_restar_company';
  $handler->display->display_options['fields']['field_restar_company']['field'] = 'field_restar_company';
  $handler->display->display_options['fields']['field_restar_company']['settings'] = array(
    'link' => 0,
  );
  /* Field: Content: Локация (GeoObject) */
  $handler->display->display_options['fields']['field_restr_location_geoobject']['id'] = 'field_restr_location_geoobject';
  $handler->display->display_options['fields']['field_restr_location_geoobject']['table'] = 'field_data_field_restr_location';
  $handler->display->display_options['fields']['field_restr_location_geoobject']['field'] = 'field_restr_location_geoobject';
  $handler->display->display_options['fields']['field_restr_location_geoobject']['exclude'] = TRUE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'restaraunt' => 'restaraunt',
  );
  $handler->display->display_options['merge_rows'] = TRUE;
  $handler->display->display_options['field_config'] = array(
    'title' => array(
      'merge_option' => 'filter',
      'separator' => ', ',
    ),
    'field_restar_company' => array(
      'merge_option' => 'filter',
      'separator' => ', ',
    ),
    'field_restr_location_geoobject' => array(
      'merge_option' => 'filter',
      'separator' => ', ',
    ),
    'title_1' => array(
      'merge_option' => 'merge',
      'separator' => '<br> ',
    ),
  );
  $handler->display->display_options['path'] = 'restaurants/table';
  $export['restaraunts'] = $view;

  return $export;
}
