<?php
/**
 * @file
 * core_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function core_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function core_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function core_feature_node_info() {
  $items = array(
    'menu_element' => array(
      'name' => t('Элемент меню'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'restaraunt' => array(
      'name' => t('Ресторан'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'suggestion' => array(
      'name' => t('Предложение'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_default_profile2_type().
 */
function core_feature_default_profile2_type() {
  $items = array();
  $items['company'] = entity_import('profile2_type', '{
    "userCategory" : false,
    "userView" : false,
    "type" : "company",
    "label" : "Company",
    "weight" : "0",
    "data" : { "registration" : 0, "use_page" : 1 },
    "rdf_mapping" : []
  }');
  $items['person'] = entity_import('profile2_type', '{
    "userCategory" : false,
    "userView" : false,
    "type" : "person",
    "label" : "Person",
    "weight" : "0",
    "data" : { "registration" : 0, "use_page" : 1 },
    "rdf_mapping" : []
  }');
  return $items;
}
