<?php
/**
 * @file
 * core_feature.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function core_feature_user_default_roles() {
  $roles = array();

  // Exported role: company.
  $roles['company'] = array(
    'name' => 'company',
    'weight' => 3,
  );

  // Exported role: person.
  $roles['person'] = array(
    'name' => 'person',
    'weight' => 4,
  );

  return $roles;
}
