<?php
/**
 * @file
 * core_feature.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function core_feature_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'edit any company profile'.
  $permissions['edit any company profile'] = array(
    'name' => 'edit any company profile',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'edit any person profile'.
  $permissions['edit any person profile'] = array(
    'name' => 'edit any person profile',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'edit own company profile'.
  $permissions['edit own company profile'] = array(
    'name' => 'edit own company profile',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'company' => 'company',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'edit own person profile'.
  $permissions['edit own person profile'] = array(
    'name' => 'edit own person profile',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'person' => 'person',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'view any company profile'.
  $permissions['view any company profile'] = array(
    'name' => 'view any company profile',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'view any person profile'.
  $permissions['view any person profile'] = array(
    'name' => 'view any person profile',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'view own company profile'.
  $permissions['view own company profile'] = array(
    'name' => 'view own company profile',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'company' => 'company',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'view own person profile'.
  $permissions['view own person profile'] = array(
    'name' => 'view own person profile',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'person' => 'person',
    ),
    'module' => 'profile2',
  );

  return $permissions;
}
